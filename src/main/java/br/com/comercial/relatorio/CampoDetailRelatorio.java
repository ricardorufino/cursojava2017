/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.relatorio;

import br.com.comercial.annotations.RufisField;

/**
 *
 * @author ricardo
 */
public class CampoDetailRelatorio {

    private final String tagInicio = "<textField>"
            + "<reportElement x=\"";
    private final int deslocamentoX;
    private final String tagMeio = "\" y=\"20\" width=\"100\" height=\"20\" uuid=\"f757f1d6-78d2-482c-9e5c-e5b9f1ee94da\"/>"
            + "<textFieldExpression><![CDATA[$F{";
    private final String nomeCampo;
    private final String tagFim = "}]]></textFieldExpression>"
            + "			</textField>";

    public CampoDetailRelatorio(RufisField rufisField) {
        this.nomeCampo = rufisField.fieldDb();
        this.deslocamentoX = rufisField.eixoX();
    }

    @Override
    public String toString() {
        return tagInicio + deslocamentoX + tagMeio + nomeCampo + tagFim;
    }
}
