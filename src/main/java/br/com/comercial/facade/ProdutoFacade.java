/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.facade;

import br.com.comercial.entidade.Produto;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author ricardo
 */
public class ProdutoFacade extends AbstractFacade<Produto> implements Serializable{

    @Inject
    private EntityManager em;
    
    public ProdutoFacade() {
        super(Produto.class);
    }

    @Override
    public EntityManager getEm() {
        return em;
    }
    
    public List<Produto> produtoLikeNome(String query){
        Query q = em.createQuery("FROM Produto AS p "
           + "WHERE LOWER(p.nome) LIKE ('%"+query.toLowerCase()+"%') OR "
           + "LOWER(p.codigo) LIKE ('%"+query.toLowerCase()+"%') "
           + "ORDER BY p.nome");
        return q.setMaxResults(20).getResultList();
    }
    
}
