/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.entidade;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 *
 * @author ricardo
 */
@Entity
@Audited
@Table(name = "pessoa_juridica")
public class PessoaJuridica extends Pessoa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "pes_cnpj", nullable = false, unique = true)
    private String cnpj;
    @Column(name = "pes_ie", nullable = false)
    private String ie;

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }

    @Override
    public String getDocumentoFederal() {
        return cnpj;
    }

    @Override
    public String getDocumentoEstadual() {
        return ie;
    }

    @Override
    public void setDocumentoFederal(String doc) {
        this.cnpj = doc;
    }

}
