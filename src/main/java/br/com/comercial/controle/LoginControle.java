/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.controle;

import br.com.comercial.entidade.Usuario;
import br.com.comercial.facade.UsuarioFacade;
import br.com.comercial.util.ControleUtil;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author ricardo
 */
@Named
@SessionScoped
public class LoginControle implements Serializable {

    @Inject
    private UsuarioFacade usuarioFacade;
    private Usuario usuario;
    private String login;
    private String senha;

    public String login() {
        try {
            usuario = usuarioFacade.findUsuario(login, senha);
            if (usuario != null) {
                ControleUtil.mensagemSistema(FacesMessage.SEVERITY_INFO, "Bem vindo ao sistema", "");
                return "app/index";
            }
        } catch (Exception e) {
            ControleUtil.mensagemSistema(FacesMessage.SEVERITY_WARN, "Erro ao fazer login", e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public String logoff() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        ControleUtil.mensagemSistema(FacesMessage.SEVERITY_INFO, "Falou valeu", "");
        return "/login";
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

}
