/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.controle;

import br.com.comercial.entidade.Venda;
import br.com.comercial.facade.AbstractFacade;
import br.com.comercial.facade.VendaFacade;
import br.com.comercial.util.ControleUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author ricardo
 */
@Named
@ViewScoped
public class VendaControle extends AbstractControle<Venda>
        implements Serializable {

    @Inject
    private VendaFacade vendaFacade;

    public VendaControle() {
        super(Venda.class);
    }

    public void relacionaPreco() {
        BigDecimal preco = super.getEntidade().getVendaItem().getProduto().getPrecoVenda();
        super.getEntidade().getVendaItem().setPreco(preco);
    }

    @Override
    public String salvar() {
        try {
            super.setEntidade(vendaFacade.salvar(super.getEntidade()));
            ControleUtil.mensagemSistema(FacesMessage.SEVERITY_INFO, "Salvo com sucesso", "");
            return "list?faces-redirect=true";
        } catch (Exception e) {
            ControleUtil.mensagemSistema(FacesMessage.SEVERITY_FATAL, "Erro ao salvar venda", e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public AbstractFacade getFacade() {
        return vendaFacade;
    }

    @Override
    public void alterar() {
        vendaFacade.carregaItenVenda(super.getEntidade());
        super.alterar();
    }

}
