/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.relatorio;

import br.com.comercial.annotations.RufisField;

/**
 *
 * @author ricardo
 */
public class CampoColumnHeader {

    private final String tagInicio = "<staticText>"
            + "<reportElement x=\"";
    private final int deslocamentoX;
    private final String tagMeio = "\" y=\"0\" width=\"100\" height=\"20\" uuid=\"b7d8a677-5b1d-4d70-873f-dd4f1dbe6082\"/>"
            + "<text><![CDATA[";
    private final String nomeCampo;
    private final String tagFim = "]]></text>"
            + "</staticText>";

    public CampoColumnHeader(RufisField rufisField) {
        this.nomeCampo = rufisField.fieldLabel();
        this.deslocamentoX = rufisField.eixoX();
    }

    @Override
    public String toString() {
        return tagInicio+deslocamentoX+tagMeio+nomeCampo+tagFim;
    }

}
