/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.relatorio;

/**
 *
 * @author ricardo
 */
public class TituloRelatorio {

    private final String tagInicio = "<title>"
            + "<band height=\"79\" splitType=\"Stretch\">"
            + "<staticText>"
            + "<reportElement x=\"0\" y=\"0\" width=\"555\" height=\"79\" uuid=\"22c90a9f-9d53-4b45-b29d-40339ae20317\"/><textElement textAlignment=\"Center\" verticalAlignment=\"Middle\">"
            + "<font size=\"26\"/>"
            + "</textElement>"
            + "<text><![CDATA[";
    private final String nomeTitulo;
    private final String tagFim = "]]></text>"
            + "</staticText>"
            + "</band>"
            + "</title>";

    public TituloRelatorio(String nomeTitulo) {
        this.nomeTitulo = nomeTitulo;
    }

    public String getNomeTitulo() {
        return nomeTitulo;
    }

    @Override
    public String toString() {
        return tagInicio+nomeTitulo+tagFim;
    }

}
