/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.relatorio;

import br.com.comercial.annotations.RufisField;

/**
 *
 * @author ricardo
 */
public class FieldsRelatorio {

    private final String nomeField;
    private final String tipo;

    public FieldsRelatorio(RufisField rufisField) {
        this.nomeField = rufisField.fieldDb();
        this.tipo = rufisField.fieldType();
    }

    @Override
    public String toString() {
        return "<field name=\""+nomeField+"\" class=\""+tipo+"\"/>";
    }

}
