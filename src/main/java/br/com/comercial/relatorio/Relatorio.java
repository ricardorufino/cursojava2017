/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.relatorio;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ricardo
 */
public class Relatorio {

    private String nomeTag = "<jasperReport xmlns=\"http://jasperreports.sourceforge.net/jasperreports\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd\" name=\"cadastral\" language=\"groovy\" pageWidth=\"595\" pageHeight=\"842\" columnWidth=\"555\" leftMargin=\"20\" rightMargin=\"20\" topMargin=\"20\" bottomMargin=\"20\" uuid=\"d738c5ff-5e54-4a20-aa11-498fff5f02a2\">";

    private final QueryRelatorio queryRelatorio;
    private final List<FieldsRelatorio> fieldsRelatorio;
    private final TituloRelatorio tituloRelatorio;
    private final PageHeaderRelatorio pageHeaderRelatorio;
    private final ColumnHeaderRelatorio columnHeaderRelatorio;
    private final DetailRelatorio detailRelatorio;

    public Relatorio(String query, String titulo, String usuario) {
        this.queryRelatorio = new QueryRelatorio(query);
        this.fieldsRelatorio = new ArrayList<>();
        this.tituloRelatorio = new TituloRelatorio(titulo);
        this.pageHeaderRelatorio = new PageHeaderRelatorio(usuario);
        this.columnHeaderRelatorio = new ColumnHeaderRelatorio();
        this.detailRelatorio = new DetailRelatorio();
    }

    public QueryRelatorio getQueryRelatorio() {
        return queryRelatorio;
    }

    public List<FieldsRelatorio> getFieldsRelatorio() {
        return fieldsRelatorio;
    }

    public TituloRelatorio getTituloRelatorio() {
        return tituloRelatorio;
    }

    public PageHeaderRelatorio getPageHeaderRelatorio() {
        return pageHeaderRelatorio;
    }

    public ColumnHeaderRelatorio getColumnHeaderRelatorio() {
        return columnHeaderRelatorio;
    }

    public DetailRelatorio getDetailRelatorio() {
        return detailRelatorio;
    }

    @Override
    public String toString() {
        String relatoiro = "";
        relatoiro += nomeTag;
        relatoiro += queryRelatorio.toString();
        for (FieldsRelatorio f : fieldsRelatorio) {
            relatoiro += f.toString();
        }
        relatoiro += tituloRelatorio.toString();
        relatoiro += columnHeaderRelatorio.toString();
        relatoiro += detailRelatorio.toString();
        return relatoiro + "</jasperReport>";
    }

}
