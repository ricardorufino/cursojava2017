/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.comercial.facade;

import br.com.comercial.entidade.Cliente;
import br.com.comercial.entidade.Produto;
import br.com.comercial.entidade.Venda;
import br.com.comercial.entidade.VendaItem;
import br.com.comercial.persistencia.Transacional;
import java.io.Serializable;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author ricardo
 */
public class VendaFacade extends AbstractFacade<Venda> implements Serializable {

    @Inject
    private EntityManager em;
    @Inject
    private ProdutoFacade produtoFacade;

    public VendaFacade() {
        super(Venda.class);
    }

    @Override
    @Transacional
    public Venda salvar(Venda entidade) throws Exception {
        try {
            entidade = super.salvar(entidade);
            for (VendaItem i : entidade.getVendaItens()) {
                Produto p = produtoFacade.pesquisarId(i.getProduto().getId());
                p.setEstoque(p.getEstoque().subtract(i.getQuantidade()));
                produtoFacade.salvar(p);
            }
            return entidade;
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public EntityManager getEm() {
        return em;
    }

    public void carregaItenVenda(Venda v) {
        v.setVendaItens(em.createQuery("FROM VendaItem AS iv WHERE iv.venda=" + v).getResultList());
    }

}
